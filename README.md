# AnimateCss Block

This module enables easy integration of Animate CSS for blocks,
allowing for streamlined animation configuration and customization.

For a complete description of the module, visit the
[project page](https://www.drupal.org/project/animatecss_block).

Submit bug reports, suggest features, or track changes in the
[issue queue](https://www.drupal.org/project/issues/animatecss_block).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

'AnimateCSS' module:
- https://www.drupal.org/project/animatecss


## Installation

Install this module as you would any contributed Drupal module.
For more information, refer to
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Block configuration form for AnimateCSS can be found in:

Administration » Structure » Block layout


## Maintainers

Current module maintainer:

- Mahyar SBT - [mahyarsbt](https://www.drupal.org/u/mahyarsbt)
