<?php

namespace Drupal\animatecss_block\Service;

use Drupal\animatecss_block\Constants\AnimateCssBlockConstants;
use Drupal\animatecss_ui\AnimateCssManagerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Block Class Service Class.
 */
class AnimateCssBlockHelperService {

  use StringTranslationTrait;

  /**
   * Animate manager.
   *
   * @var \Drupal\animatecss_ui\AnimateCssManagerInterface
   */
  protected $animateManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Construct of Block Class service.
   *
   * @param \Drupal\animatecss_ui\AnimateCssManagerInterface $animate_manager
   *   The Animate selector manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   Account proxy.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(AnimateCssManagerInterface $animate_manager, ConfigFactoryInterface $config_factory, Connection $database, AccountProxyInterface $account_proxy, TimeInterface $time) {
    $this->animateManager = $animate_manager;
    $this->configFactory = $config_factory;
    $this->connection = $database;
    $this->currentUser = $account_proxy;
    $this->time = $time;
  }

  /**
   * Method to do the presave block.
   */
  public function animateCssBlockPreSave(&$entity) {
    // Get the config object.
    $config = $this->configFactory->getEditable('animatecss_block.settings');

    // Get the current AnimateCSS blocks stored.
    $animatecss_blocks_stored = $config->get('blocks');

    $animatecss_status = $entity->getThirdPartySetting('animatecss_block', 'status');

    // Get the current block ID.
    $animatecss_block_id = $entity->id();

    // Gets the block CSS selector.
    $selector = '#block-' . strtr($animatecss_block_id ?? '', '_', '-');

    $animate_id = 0;
    if (!is_null($animatecss_block_id) && $isAnimate = $this->animateManager->isAnimate($selector)) {
      $animate_id = $this->connection->query("SELECT [aid] FROM {animatecss} WHERE [selector] = :selector", [':selector' => $selector])
        ->fetchField();
    }

    // If AnimateCSS is not enabled in the block, unset the ThirdPartySetting.
    if (!$animatecss_status) {
      $entity->unsetThirdPartySetting('animatecss_block', 'status');
      $entity->unsetThirdPartySetting('animatecss_block', 'animation');
      $entity->unsetThirdPartySetting('animatecss_block', 'delay');
      $entity->unsetThirdPartySetting('animatecss_block', 'time');
      $entity->unsetThirdPartySetting('animatecss_block', 'speed');
      $entity->unsetThirdPartySetting('animatecss_block', 'duration');
      $entity->unsetThirdPartySetting('animatecss_block', 'repeat');
      $entity->unsetThirdPartySetting('animatecss_block', 'event');
      $entity->unsetThirdPartySetting('animatecss_block', 'once');
      $entity->unsetThirdPartySetting('animatecss_block', 'clean');
      $entity->unsetThirdPartySetting('animatecss_block', 'display');

      if ($isAnimate && !empty($animate_id)) {
        $this->animateManager->removeAnimate($animate_id);

        if (($key = array_search($animatecss_block_id, $animatecss_blocks_stored)) !== FALSE) {
          unset($animatecss_blocks_stored[$key]);

          // Store in the config.
          $config->set('blocks', $animatecss_blocks_stored);

          // Save.
          $config->save();
        }
      }
    }
    else {
      // Get the AnimateCSS block ThirdPartySetting.
      $settings = [
        'animation' => $entity->getThirdPartySetting('animatecss_block', 'animation'),
        'delay'     => $entity->getThirdPartySetting('animatecss_block', 'delay'),
        'time'      => $entity->getThirdPartySetting('animatecss_block', 'time'),
        'speed'     => $entity->getThirdPartySetting('animatecss_block', 'speed'),
        'duration'  => $entity->getThirdPartySetting('animatecss_block', 'duration'),
        'repeat'    => $entity->getThirdPartySetting('animatecss_block', 'repeat'),
        'event'     => $entity->getThirdPartySetting('animatecss_block', 'event'),
        'once'      => $entity->getThirdPartySetting('animatecss_block', 'once'),
        'clean'     => $entity->getThirdPartySetting('animatecss_block', 'clean'),
        'display'   => $entity->getThirdPartySetting('animatecss_block', 'display'),
      ];

      if ($wow = $entity->getThirdPartySetting('animatecss_block', 'wow')) {
        $settings['wow'] = $wow;
      }

      if ($aos = $entity->getThirdPartySetting('animatecss_block', 'aos')) {
        $settings['aos'] = $aos;
      }

      // Set the AnimateCSS block ThirdPartySetting.
      $entity->setThirdPartySetting('animatecss_block', 'status', $animatecss_status);
      $entity->setThirdPartySetting('animatecss_block', 'animation', $settings['animation']);
      $entity->setThirdPartySetting('animatecss_block', 'delay', $settings['delay']);
      $entity->setThirdPartySetting('animatecss_block', 'time', $settings['time']);
      $entity->setThirdPartySetting('animatecss_block', 'speed', $settings['speed']);
      $entity->setThirdPartySetting('animatecss_block', 'duration', $settings['duration']);
      $entity->setThirdPartySetting('animatecss_block', 'repeat', $settings['repeat']);
      $entity->setThirdPartySetting('animatecss_block', 'event', $settings['event']);
      $entity->setThirdPartySetting('animatecss_block', 'once', $settings['once']);
      $entity->setThirdPartySetting('animatecss_block', 'clean', $settings['clean']);
      $entity->setThirdPartySetting('animatecss_block', 'display', $settings['display']);

      if ($wow) {
        $entity->setThirdPartySetting('animatecss_block', 'wow', $settings['wow']);
      }

      if ($aos) {
        $entity->setThirdPartySetting('animatecss_block', 'aos', $settings['aos']);
      }

      $label = 'AnimateCSS Block - ' . ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $animatecss_block_id)));
      $comment = 'This AnimateCSS related to block configuration.';

      // Set variables from main animate settings.
      $variables['animation'] = $settings['animation'];
      $variables['delay']     = $settings['delay'];
      $variables['time']      = $settings['time'];
      $variables['speed']     = $settings['speed'];
      $variables['duration']  = $settings['duration'];
      $variables['repeat']    = $settings['repeat'];
      $variables['clean']     = $settings['clean'];
      $variables['display']   = $settings['display'];
      $variables['event']     = $settings['event'];
      $variables['once']      = $settings['once'];

      // Get variables from other module.
      if ($wow) {
        $variables['wow'] = $wow;
      }

      if ($aos) {
        $variables['aos'] = $aos;
      }

      // Serialize options variables.
      $options = serialize($variables);

      // The Unix timestamp when the animate was most recently saved.
      $changed = $this->time->getCurrentTime();

      // Save animate.
      $this->animateManager->addAnimate($animate_id, $selector, $label, $comment, $changed, $animatecss_status, $options);

      if (!in_array($animatecss_block_id, $animatecss_blocks_stored)) {
        // Merge with the current one.
        array_push($animatecss_blocks_stored, $animatecss_block_id);
        $animatecss_blocks_to_store = array_unique($animatecss_blocks_stored);

        // Store in the config.
        $config->set('blocks', $animatecss_blocks_to_store);

        // Save.
        $config->save();
      }
    }

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();
  }

  /**
   * Method to do a form alter.
   */
  public function animateCssBlockFormAlter(&$form, &$form_state) {
    // If the user don't have permission, skip that.
    if (!$this->currentUser->hasPermission(AnimateCssBlockConstants::ANIMATECSS_BLOCK_PERMISSION)) {
      return;
    }

    $form_object = $form_state->getFormObject();

    // Implement the alter only if is an instance of EntityFormInterface.
    if (!($form_object instanceof EntityFormInterface)) {
      return;
    }

    /** @var \Drupal\block\BlockInterface $block */
    $block = $form_object->getEntity();

    // Get the current block ID.
    $animatecss_block_id = $block->id();

    // Gets the block CSS selector.
    $selector = '#block-' . strtr($animatecss_block_id ?? '', '_', '-');

    if (!is_null($animatecss_block_id) && !$this->animateManager->isAnimate($selector)) {
      $block->unsetThirdPartySetting('animatecss_block', 'status');
      $block->unsetThirdPartySetting('animatecss_block', 'animation');
      $block->unsetThirdPartySetting('animatecss_block', 'delay');
      $block->unsetThirdPartySetting('animatecss_block', 'time');
      $block->unsetThirdPartySetting('animatecss_block', 'speed');
      $block->unsetThirdPartySetting('animatecss_block', 'duration');
      $block->unsetThirdPartySetting('animatecss_block', 'repeat');
      $block->unsetThirdPartySetting('animatecss_block', 'event');
      $block->unsetThirdPartySetting('animatecss_block', 'once');
      $block->unsetThirdPartySetting('animatecss_block', 'clean');
      $block->unsetThirdPartySetting('animatecss_block', 'display');
      $block->unsetThirdPartySetting('animatecss_block', 'wow');
      $block->unsetThirdPartySetting('animatecss_block', 'aos');
    }

    // Load the Animate.css configuration settings.
    $config_animatecss = $this->configFactory->getEditable('animatecss.settings');

    // Attach Animate.css to pages with chosen method.
    $method = animatecss_check_installed() ? $config_animatecss->get('method') : 'cdn';
    $compat = $config_animatecss->get('compat');

    // Attach Animate.css for examples.
    $sample = $config_animatecss->get('options');
    $sample['selector'] = '.animate__sample';

    // Attach AnimateCSS Block library.
    $form['#attached']['library'][] = 'animatecss_block/animate-block';
    $form['#attached']['drupalSettings']['animatecss']['sample'] = $sample;
    $form['#attached']['drupalSettings']['animatecss']['compat'] = $compat;

    if ($method == 'cdn') {
      $form['#attached']['library'][] = 'animatecss_ui/animate-cdn-dev';
    }
    else {
      $form['#attached']['library'][] = 'animatecss_ui/animate-dev';
    }

    // Get AnimateCSS Block config object.
    $config = $this->configFactory->getEditable('animatecss_block.settings');

    // Get AnimateCSS block settings default value.
    $options = [
      'status'    => $block->getThirdPartySetting('animatecss_block', 'status', $config->get('status')),
      'animation' => $block->getThirdPartySetting('animatecss_block', 'animation', $config_animatecss->get('options.animation')),
      'delay'     => $block->getThirdPartySetting('animatecss_block', 'delay', $config_animatecss->get('options.delay')),
      'time'      => $block->getThirdPartySetting('animatecss_block', 'time', $config_animatecss->get('options.time')),
      'speed'     => $block->getThirdPartySetting('animatecss_block', 'speed', $config_animatecss->get('options.speed')),
      'duration'  => $block->getThirdPartySetting('animatecss_block', 'duration', $config_animatecss->get('options.duration')),
      'repeat'    => $block->getThirdPartySetting('animatecss_block', 'repeat', $config_animatecss->get('options.repeat')),
      'event'     => $block->getThirdPartySetting('animatecss_block', 'event', $config_animatecss->get('options.event')),
      'once'      => $block->getThirdPartySetting('animatecss_block', 'once', $config_animatecss->get('options.once')),
      'clean'     => $block->getThirdPartySetting('animatecss_block', 'clean', $config_animatecss->get('options.clean')),
      'display'   => $block->getThirdPartySetting('animatecss_block', 'display', $config_animatecss->get('options.display')),
    ];

    if ($wow = $block->getThirdPartySetting('animatecss_block', 'wow', $config_animatecss->get('options.wow'))) {
      $options['wow'] = $wow;
    }

    if ($aos = $block->getThirdPartySetting('animatecss_block', 'aos', $config_animatecss->get('options.aos'))) {
      $options['aos'] = $aos;
    }

    // Get the URL settings global page.
    $url_settings_page = Url::fromRoute('animatecss_block.settings')->toString();
    $txt_settings_page = $this->t('If you need to change the configuration and defaults, please visit the <a href="@url_settings_page">settings page</a>.', ['@url_settings_page' => $url_settings_page]);

    // Put the default help text.
    $help_text = $this->t('Animate this block by configuring the Animate CSS options.');

    // AnimateCSS flags for specific form labels and suffix.
    $experimental_label = ' <span class="animatecss-experimental-flag">Experimental</span>';
    $beta_label = ' <span class="animatecss-beta-flag">Beta</span>';
    $new_label = ' <span class="animatecss-new-flag">New</span>';
    $ms_unit_label = ' <span class="animatecss-unit-flag">ms</span>';

    $form['animatecss'] = [
      '#type'        => 'details',
      '#title'       => $this->t('AnimateCSS'),
      '#open'        => TRUE,
      '#description' => $help_text,
      '#weight'      => 0,
      '#attributes'  => [
        'class' => ['animatecss-block-details'],
      ],
    ];

    // Put the weight only if exists.
    if (!empty($config->get('weight')) || $config->get('weight') !== '0') {
      $form['animatecss']['#weight'] = $config->get('weight');
    }

    // This will automatically be saved in the third party settings.
    $form['animatecss']['third_party_settings']['#tree'] = TRUE;

    // Enabled status for this animate.
    $form['animatecss']['third_party_settings']['animatecss_block']['status'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enabled'),
      '#default_value' => $options['status'] ?? FALSE,
    ];

    // AnimateCSS fields wrapper.
    $form['animatecss']['third_party_settings']['animatecss_block']['container'] = [
      '#type'   => 'container',
      '#states' => [
        'visible' => [
          'input[name="animatecss[third_party_settings][animatecss_block][status]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // The animation to use.
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['animation'] = [
      '#title'         => $this->t('Animation'),
      '#type'          => 'select',
      '#options'       => animatecss_animation_options(),
      '#default_value' => $options['animation'] ?? $config_animatecss->get('options.animation'),
      '#description'   => $this->t('Select the animation name you want to use for CSS selectors globally.'),
      '#attributes'    => ['class' => ['animate__animation']],
    ];

    // Animate.css provides the following delays.
    // Animate duration used as a prefix on CSS Variables.
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['delay_wrapper'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => ['animatecss-wrapper', 'container-inline', 'form-item'],
      ],
    ];
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['delay_wrapper']['delay'] = [
      '#title'         => $this->t('Delay'),
      '#type'          => 'select',
      '#options'       => animatecss_delay_options(),
      '#default_value' => $options['delay'] ?? $config_animatecss->get('options.delay'),
      '#attributes'    => ['class' => ['animate__delay']],
    ];
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['delay_wrapper']['time'] = [
      '#type'          => 'number',
      '#min'           => 0,
      '#title'         => $this->t('Delay time'),
      '#default_value' => $options['time'] ?? $config_animatecss->get('options.time'),
      '#field_suffix'  => $ms_unit_label,
      '#attributes'    => ['class' => ['animate__time', 'animate-delay-time']],
      '#states'        => [
        'visible' => [
          'select[name="animatecss[third_party_settings][animatecss_block][container][delay_wrapper][delay]"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['delay_wrapper']['time_description'] = [
      '#type'   => 'markup',
      '#markup' => $this->t('The provided delays are from 1 to 5 seconds. You can customize them by selecting custom and set the delay time directly on the elements to delay the start of the animation.'),
      '#prefix' => '<div class="form-item__description">',
      '#suffix' => '</div>',
    ];

    // Animate speed time and duration used as a prefix on CSS Variables.
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['duration_wrapper'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => ['animatecss-wrapper', 'container-inline', 'form-item'],
      ],
    ];
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['duration_wrapper']['speed'] = [
      '#title'         => $this->t('Speed'),
      '#type'          => 'select',
      '#options'       => animatecss_speed_options(),
      '#default_value' => $options['speed'] ?? $config_animatecss->get('options.speed'),
      '#attributes'    => ['class' => ['animate__speed']],
    ];
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['duration_wrapper']['duration'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Duration'),
      '#default_value' => $options['duration'] ?? $config_animatecss->get('options.duration'),
      '#field_suffix'  => $ms_unit_label,
      '#attributes'    => ['class' => ['animate__duration', 'animate-duration']],
      '#states'        => [
        'visible' => [
          'select[name="animatecss[third_party_settings][animatecss_block][container][duration_wrapper][speed]"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['duration_wrapper']['speed_description'] = [
      '#type'   => 'markup',
      '#markup' => $this->t('You can control the speed of the animation. The medium option speed is 1 second same as a default speed. You can also set the animations duration by selecting customize option.'),
      '#prefix' => '<div class="form-item__description">',
      '#suffix' => '</div>',
    ];

    // Animate iteration count.
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['repeat'] = [
      '#title'         => $this->t('Repeating'),
      '#type'          => 'select',
      '#options'       => animatecss_repeat_options(),
      '#default_value' => $options['repeat'] ?? $config_animatecss->get('options.repeat'),
      '#description'   => $this->t('You can control the iteration count of the animation.'),
      '#attributes'    => ['class' => ['animate__repeat']],
    ];

    // Clean previous animation classes.
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['clean'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Clean previous classes') . $beta_label,
      '#description'   => $this->t("Previous Animate.css classes may already be applied to this element from the site's theme. Selecting this option will remove them before applying the new classes based on the settings on this page."),
      '#default_value' => $options['clean'] ?? $config_animatecss->get('options.clean'),
      '#weight'        => 90,
    ];

    // Convert element display if is inline.
    $form['animatecss']['third_party_settings']['animatecss_block']['container']['display'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Fix Display') . $experimental_label,
      '#description'   => $this->t("Enable this option to change display from inline to inline-block to fix animation issues."),
      '#default_value' => $options['display'] ?? $config_animatecss->get('options.display'),
      '#weight'        => 96,
    ];

    $form['animatecss']['third_party_settings']['animatecss_block']['container']['help_text_items'] = [
      '#type' => 'markup',
      '#markup' => '<p class="help-text-setting-items' . (!$config->get('form.settings_link') ? ' help-text-settings-items-hidden' : '') . '">' . $txt_settings_page . '</p>',
      '#weight' => 99,
    ];

    $form['animatecss']['third_party_settings']['animatecss_block']['region'] = [
      '#type'       => 'container',
      '#attributes' => [
        'id'    => 'animatecss_sidebar',
        'class' => ['animatecss-secondary-region'],
      ],
    ];

    // Sidebar close button.
    $close_sidebar_translation = $this->t('Close sidebar panel');
    $form['animatecss']['third_party_settings']['animatecss_block']['region']['sidebar_close'] = [
      '#markup' => '<a href="#close-sidebar" class="toggle-sidebar__close trigger" role="button" title="' . $close_sidebar_translation . '"><span class="visually-hidden">' . $close_sidebar_translation . '</span></a>',
    ];

    // Animate.css preview.
    $form['animatecss']['third_party_settings']['animatecss_block']['region']['preview'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => ['animate__container-preview']],
    ];
    $rebuild_preview = $this->t('Rebuild');
    // Animate.css animation preview.
    $form['animatecss']['third_party_settings']['animatecss_block']['region']['preview']['sample'] = [
      '#type'   => 'markup',
      '#markup' => '<div class="animate__preview"><p class="animate__sample">Animate CSS</p></div>',
      '#suffix' => '<a href="#rebuild-preview" class="animate__replay" role="button" title="' . $rebuild_preview . '" aria-controls="animatecss_sidebar"><span class="visually-hidden">' . $rebuild_preview . '</span></a>',
    ];

    // The jQuery event options.
    $form['animatecss']['third_party_settings']['animatecss_block']['region']['event_options'] = [
      '#title' => $this->t('Events') . $experimental_label,
      '#type' => 'details',
      '#open' => TRUE,
      '#attributes' => ['class' => ['animate__event-options']],
      '#states' => [
        'disabled' => [
          [
            ':input[name="animatecss[third_party_settings][animatecss_block][region][wow_options][wow]"]' => ['checked' => TRUE],
          ],
          [
            ':input[name="animatecss[third_party_settings][animatecss_block][region][aos_options][aos]"]' => ['checked' => TRUE],
          ],
        ],
      ],
    ];
    $events = animatecss_event_options();
    $form['animatecss']['third_party_settings']['animatecss_block']['region']['event_options']['event'] = [
      '#type'          => 'select',
      '#options'       => $events,
      '#title'         => $this->t('Event'),
      '#title_display' => 'invisible',
      '#description'   => $this->t('Select the event to trigger the Animate.css animation and will be automatically handled by JavaScript. This field allows you to customize when the animation occurs.'),
      '#default_value' => $options['event'] ?? $config->get('options.event'),
      '#attributes'    => ['class' => ['animate__event']],
      '#field_suffix'  => $new_label,
    ];

    // Trigger the animation only once.
    $form['animatecss']['third_party_settings']['animatecss_block']['region']['event_options']['once'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Once'),
      '#description'   => $this->t("Trigger the animation by the specified event only once."),
      '#default_value' => $options['once'] ?? $config->get('options.once'),
      '#attributes'    => ['class' => ['animate__once']],
      '#states'        => [
        'invisible' => [
          'select[name="animatecss[third_party_settings][animatecss_block][region][event_options][event]"]' => ['value' => 'load'],
        ],
      ],
    ];

    $scroll_options = animatecss_scroll_options($options);
    if (count($scroll_options)) {
      foreach ($scroll_options as $library => $info) {
        // The scroll library options.
        $form['animatecss']['third_party_settings']['animatecss_block']['region'][$library . '_options'] = [
          '#title' => $this->t('@library options', ['@library' => $info['name']]),
          '#type'  => 'details',
          '#open'  => TRUE,
        ];

        // Enable library to Animate On Scroll.
        $form['animatecss']['third_party_settings']['animatecss_block']['region'][$library . '_options'][$library] = [
          '#type'          => 'checkbox',
          '#title'         => $this->t('Enable @library', ['@library' => $info['name']]),
          '#description'   => $this->t('@description', ['@description' => $info['description']]),
          '#default_value' => $options[$library]['enable'] ?? FALSE,
          '#attributes'    => ['class' => ['animate__scroll']],
        ];

        // The library setting options.
        if (isset($info['fields']) && count($info['fields'])) {
          $form['animatecss']['third_party_settings']['animatecss_block']['region'][$library . '_options']['setting'] = [
            '#type' => 'container',
            '#states' => [
              'disabled' => [
                ':input[name="animatecss[third_party_settings][animatecss_block][region][' . $library . '_options][' . $library . ']"]' => ['checked' => FALSE],
              ],
              'visible' => [
                ':input[name="animatecss[third_party_settings][animatecss_block][region][' . $library . '_options][' . $library . ']"]' => ['checked' => TRUE],
              ],
            ],
          ];
          foreach ($info['fields'] as $field => $data) {
            $form['animatecss']['third_party_settings']['animatecss_block']['region'][$library . '_options']['setting'][$field] = $data;
          }
        }
      }
    }

    if (!$config->get('form.description')) {
      $form['animatecss']['#attributes']['class'][] = 'animatecss-block-no-description';
    }
    if (!$config->get('field.delay')) {
      $form['animatecss']['third_party_settings']['animatecss_block']['container']['delay_wrapper']['#attributes']['class'][] = 'animatecss-block-item-hidden';
    }
    if (!$config->get('field.speed')) {
      $form['animatecss']['third_party_settings']['animatecss_block']['container']['duration_wrapper']['#attributes']['class'][] = 'animatecss-block-item-hidden';
    }
    if (!$config->get('field.repeat')) {
      $form['animatecss']['third_party_settings']['animatecss_block']['container']['repeat']['#wrapper_attributes']['class'][] = 'animatecss-block-item-hidden';
    }
    if (!$config->get('feature.clean')) {
      $form['animatecss']['third_party_settings']['animatecss_block']['container']['clean']['#wrapper_attributes']['class'][] = 'animatecss-block-item-hidden';
    }
    if (!$config->get('feature.display')) {
      $form['animatecss']['third_party_settings']['animatecss_block']['container']['display']['#wrapper_attributes']['class'][] = 'animatecss-block-item-hidden';
    }
    if (!$config->get('feature.preview')) {
      $form['animatecss']['third_party_settings']['animatecss_block']['region']['preview']['#attributes']['class'][] = 'animatecss-block-item-hidden';
    }
    if (!$config->get('feature.event')) {
      $form['animatecss']['third_party_settings']['animatecss_block']['region']['event_options']['#attributes']['class'][] = 'animatecss-block-item-hidden';
    }
    if (!$config->get('feature.preview') && !$config->get('feature.event')) {
      $form['animatecss']['third_party_settings']['animatecss_block']['region']['#attributes']['class'][] = 'animatecss-block-item-hidden';
    }
    else {
      // Add sidebar toggle.
      $hide_panel = $this->t('Hide sidebar panel');
      $form['animatecss']['third_party_settings']['animatecss_block']['sticky']['sidebar_toggle'] = [
        '#markup' => '<a href="#toggle-sidebar" class="toggle-sidebar__trigger trigger" role="button" title="' . $hide_panel . '" aria-controls="animatecss_sidebar"><span class="visually-hidden">' . $hide_panel . '</span></a>',
        '#weight' => '999',
      ];
      $form['animatecss']['third_party_settings']['animatecss_block']['sidebar_overlay'] = [
        '#markup' => '<div class="toggle-sidebar__overlay trigger"></div>',
      ];
    }

    $form['#validate'][] = 'animatecss_block_form_block_form_validate';
    $form['actions']['submit']['#submit'][] = 'animatecss_block_form_block_form_submit';
  }

  /**
   * Method to do a form validate.
   */
  public function animateCssBlockFormValidate(&$form, &$form_state) {
    // Get the ThirdPartySettings.
    $animatecss = $form_state->getValue('animatecss') ? $form_state->getValue('animatecss')['third_party_settings']['animatecss_block'] : [];

    // Clear empty values.
    if (count($animatecss)) {
      $animatecss_block = [
        'status'    => $animatecss['status'],
        'animation' => $animatecss['container']['animation'],
        'delay'     => $animatecss['container']['delay_wrapper']['delay'],
        'time'      => $animatecss['container']['delay_wrapper']['time'],
        'speed'     => $animatecss['container']['duration_wrapper']['speed'],
        'duration'  => $animatecss['container']['duration_wrapper']['duration'],
        'repeat'    => $animatecss['container']['repeat'],
        'clean'     => $animatecss['container']['clean'],
        'display'   => $animatecss['container']['display'],
        'event'     => $animatecss['region']['event_options']['event'],
        'once'      => $animatecss['region']['event_options']['once'],
      ];

      // Set wow variables.
      if (!empty($animatecss['region']['wow_options']['wow'])) {
        $animatecss_block['wow'] = [
          'enable' => $animatecss['region']['wow_options']['wow'],
          'once'   => $animatecss['region']['wow_options']['setting']['once'],
          'mirror' => $animatecss['region']['wow_options']['setting']['mirror'],
        ];
      }

      // Set aos variables.
      if (!empty($animatecss['region']['aos_options']['aos'])) {
        // Provides delay value for AOS from
        // AnimateCSS delay and time options.
        switch ($animatecss['container']['delay_wrapper']['delay']) {
          case 'delay-1s':
            $delay = '1000';
            break;

          case 'delay-2s':
            $delay = '2000';
            break;

          case 'delay-3s':
            $delay = '3000';
            break;

          case 'delay-4s':
            $delay = '4000';
            break;

          case 'delay-5s':
            $delay = '5000';
            break;

          default:
            if ($animatecss['container']['delay_wrapper']['delay'] == 'custom' && $animatecss['container']['delay_wrapper']['time'] > 0) {
              $delay = $animatecss['container']['delay_wrapper']['time'];
            }
            else {
              $delay = '0';
            }
        }

        // Provides duration value for AOS from
        // AnimateCSS speed and duration options.
        switch ($animatecss['container']['duration_wrapper']['speed']) {
          case 'slower':
            $duration = '3000';
            break;

          case 'slow':
            $duration = '2000';
            break;

          case 'fast':
            $duration = '800';
            break;

          case 'faster':
            $duration = '500';
            break;

          default:
            if ($animatecss['container']['duration_wrapper']['speed'] == 'custom' && !empty($animatecss['container']['duration_wrapper']['duration'])) {
              $duration = $animatecss['container']['duration_wrapper']['duration'];
            }
            else {
              $duration = '1000';
            }
        }

        $animatecss_block['aos'] = [
          'enable'   => $animatecss['region']['aos_options']['aos'],
          'offset'   => $animatecss['region']['aos_options']['setting']['aos_offset'],
          'easing'   => $animatecss['region']['aos_options']['setting']['easing'],
          'delay'    => $delay,
          'duration' => $duration,
          'once'     => $animatecss['region']['aos_options']['setting']['once'],
          'mirror'   => $animatecss['region']['aos_options']['setting']['mirror'],
        ];
      }

      $third_party_settings['animatecss_block'] = array_filter($animatecss_block);
    }

    // Set the ThirdPartySettings with the default array.
    $form_state->setValue('third_party_settings', $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public function animateCssBlockFormSubmit(&$form, &$form_state) {

    // Get the ThirdPartySettings.
    $animatecss = $form_state->getValue('animatecss') ? $form_state->getValue('animatecss')['third_party_settings']['animatecss_block'] : [];

    // Check if animate css enabled.
    if (count($animatecss) && $animatecss['status']) {
      // Get the config object.
      $config = $this->configFactory->getEditable('animatecss_block.settings');

      // Get the current block ID.
      $animatecss_block_id = $form_state->getValue('id');

      // Gets the block CSS selector.
      $selector = '#block-' . strtr($animatecss_block_id, '_', '-');

      // Gets the animate CSS ID.
      $animate_id = 0;
      if ($isAnimate = $this->animateManager->isAnimate($selector)) {
        $animate_id = $this->connection->query("SELECT [aid] FROM {animatecss} WHERE [selector] = :selector", [':selector' => $selector])
          ->fetchField();
      }

      // A destination was set, probably on an exception controller.
      if ($isAnimate && $config->get('form.redirect')) {
        if ($config->get('form.redirect') == 'edit' && $animate_id) {
          $form_state->setRedirect(
            'animatecss.edit',
            ['animate' => $animate_id]
          );
        }
        else {
          $form_state->setRedirect('animatecss.admin');
        }

        $form_state->setIgnoreDestination();
      }
    }

  }

}
