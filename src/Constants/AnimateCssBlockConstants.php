<?php

namespace Drupal\animatecss_block\Constants;

/**
 * AnimateCSS Block Constants.
 */
class AnimateCssBlockConstants {

  /**
   * AnimateCSS Block Permission.
   */
  const ANIMATECSS_BLOCK_PERMISSION = 'administer animate css block';

}
