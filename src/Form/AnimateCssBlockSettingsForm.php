<?php

namespace Drupal\animatecss_block\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for AnimateCSS Block Settings.
 */
class AnimateCssBlockSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates the construct.
   */
  public function __construct(RouteMatchInterface $route_match, ModuleHandlerInterface $moduleHandler, ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    $this->routeMatch = $route_match;
    $this->moduleHandler = $moduleHandler;
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'animatecss_block_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'animatecss_block.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get AnimateCss Block config object.
    $config = $this->config('animatecss_block.settings');

    // Form settings for the block configuration.
    $form['form_settings'] = [
      '#title' => $this->t('Form'),
      '#description'   => $this->t('Optimizing and enhancing the performance of the animation configuration form in block settings.'),
      '#type' => 'details',
      '#open' => TRUE,
    ];
    $form['form_settings']['redirect'] = [
      '#title' => $this->t("Redirect"),
      '#description' => $this->t("By default, saving the block configuration redirects to the block list. Enable this option to be redirected to the animations list or the block's animation edit form if Animate CSS is enabled."),
      '#type' => 'checkbox',
      '#default_value' => $config->get('form.redirect'),
    ];
    $form['form_settings']['destination'] = [
      '#title' => $this->t("Redirect destination"),
      '#description' => $this->t('The "Animate List" option displays animations created for blocks and other elements, while the "Edit Form" is useful when you have limited the settings fields in the block configuration form.'),
      '#type' => 'select',
      '#options' => [
        'list' => $this->t('Animate list'),
        'edit' => $this->t('Edit form'),
      ],
      '#default_value' => $config->get('form.destination'),
      '#states' => [
        'visible' => [
          ':input[name="redirect"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['form_settings']['description'] = [
      '#title' => $this->t("Field descriptions"),
      '#description' => $this->t('If you are familiar with Animate CSS settings, you can disable the field descriptions to make the settings form more streamlined.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('form.description'),
    ];
    $form['form_settings']['settings_link'] = [
      '#title' => $this->t("Settings link"),
      '#description' => $this->t('Disabling this option hides the description text and settings link at the bottom of the form.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('form.settings_link'),
    ];

    // Field settings in the block configuration form.
    $form['field_settings'] = [
      '#title' => $this->t('Fields'),
      '#description'   => $this->t('Show or hide the necessary animation settings fields in the block configuration form.'),
      '#type' => 'details',
      '#open' => TRUE,
    ];
    $form['field_settings']['delay'] = [
      '#title' => $this->t("Delay"),
      '#description' => $this->t('Enable this field to select custom and set the delay time directly on elements to delay the animation start.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('field.delay'),
    ];
    $form['field_settings']['speed'] = [
      '#title' => $this->t("Speed"),
      '#description' => $this->t('Enable this field to customize the animation duration.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('field.speed'),
    ];
    $form['field_settings']['repeat'] = [
      '#title' => $this->t("Repeating"),
      '#description' => $this->t('Enable this field to control the iteration count of the animation.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('field.repeat'),
    ];

    // Feature settings in the block configuration form.
    $form['feature_settings'] = [
      '#title' => $this->t('Features'),
      '#description' => $this->t('Customize features in the block configuration section based on your needs and usage.'),
      '#type' => 'details',
      '#open' => TRUE,
    ];
    $form['feature_settings']['clean'] = [
      '#title' => $this->t("Clean previous classes"),
      '#description' => $this->t('Enable this feature for remove old classed before applying the new classes.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('feature.clean'),
    ];
    $form['feature_settings']['display'] = [
      '#title' => $this->t("Fix Display"),
      '#description' => $this->t('Enable this feature for change display from inline to inline-block to fix animation issues.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('feature.display'),
    ];
    $form['feature_settings']['preview'] = [
      '#title' => $this->t("Preview"),
      '#description' => $this->t('Enable this feature to display the preview section and rebuild button to see the animation effect.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('feature.preview'),
    ];
    $form['feature_settings']['event'] = [
      '#title' => $this->t("Events"),
      '#description' => $this->t('Enable this feature to customize when the animation occurs by JavaScript events.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('feature.event'),
    ];

    if ($this->moduleHandler->moduleExists('wowjs_ui')) {
      $form['feature_settings']['wow'] = [
        '#title' => $this->t("WOW"),
        '#description' => $this->t('Enable this feature to reveal CSS animation as you scroll down a page.'),
        '#type' => 'checkbox',
        '#default_value' => $config->get('feature.wow'),
      ];
    }

    if ($this->moduleHandler->moduleExists('animatecss_aos')) {
      $form['feature_settings']['aos'] = [
        '#title' => $this->t("AOS"),
        '#description' => $this->t('Enable this feature to provides Animate On Scroll.'),
        '#type' => 'checkbox',
        '#default_value' => $config->get('feature.aos'),
      ];
    }

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    // Get the config object.
    $config = $this->config('animatecss_block.settings');

    // Set the values.
    $config->set('form.redirect', $values['redirect'] !== 0);
    $config->set('form.destination', $values['destination']);
    $config->set('form.description', $values['description'] !== 0);
    $config->set('form.settings_link', $values['settings_link'] !== 0);
    $config->set('field.delay', $values['delay'] !== 0);
    $config->set('field.speed', $values['speed'] !== 0);
    $config->set('field.repeat', $values['repeat'] !== 0);
    $config->set('feature.clean', $values['clean'] !== 0);
    $config->set('feature.display', $values['display'] !== 0);
    $config->set('feature.preview', $values['preview'] !== 0);
    $config->set('feature.event', $values['event'] !== 0);
    $config->set('feature.wow', isset($values['wow']) && $values['wow'] !== 0 ?? TRUE);
    $config->set('feature.aos', isset($values['aos']) && $values['aos'] !== 0 ?? TRUE);

    // Save that.
    $config->save();

    parent::submitForm($form, $form_state);

  }

}
