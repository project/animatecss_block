/**
 * @file
 * Contains definition of the behaviour Animate.css.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  Drupal.behaviors.animateCssBlock = {
    attach: function (context, settings) {

      if ( once('animateCssBlockEvent', '.animate__event').length ) {
        const scrollLibraries = $('.animate__scroll');

        scrollLibraries.bind({
          click: function (e) {
            let currentLibrary = $(this);

            if ( currentLibrary.is(':checked') ) {
              $('.animate__event-options').removeAttr('open');
              $('.animate__event').val("load").change();
              scrollLibraries.not(this).prop({ disabled: true, checked: false });
              scrollLibraries.not(this).closest('.form-wrapper').removeAttr('open');
            }
            else {
              $('.animate__event-options').attr('open', 'open');
              scrollLibraries.not(this).prop({ disabled: false, checked: false });
              scrollLibraries.not(this).closest('.form-wrapper').attr('open', 'open');
            }
          },
        });
      }

    }
  };

})(jQuery, Drupal, drupalSettings, once);
